# Accordion and Theme Changer

[![Pipeline](https://gitlab.com/mfaaach/story7/badges/master/pipeline.svg)](https://gitlab.com/mfaaach/story7/pipelines)
[![coverage report](https://gitlab.com/mfaaach/story7/badges/master/coverage.svg)](https://gitlab.com/mfaaach/story7/commits/master)


Fachri's PPW Lab 7 Project 

## URL

This lab projects can be accessed from [https://story7pow.herokuapp.com](https://story7pow.herokuapp.com)

## Authors

* **Muhammad Fachri Anandito** - [mfaaach](https://gitlab.com/mfaaach)
